<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array(
                    array(
                        'nome' => 'Data da Nota',
                        'dominio' => 'Entrada',
                        'coluna' => 'data',
                        'tipo' => 'date'
                    ),
                    array(
                        'nome' => 'Responsável',
                        'dominio' => 'Entrada',
                        'coluna' => 'usuario_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Usuario', 'coluna' => 'nome'),
                        'input' => false
                    ),
                    array(
                        'nome' => 'Número da Nota',
                        'dominio' => 'Entrada',
                        'coluna' => 'nota',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Produto',
                        'dominio' => 'Entrada',
                        'coluna' => 'produto_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Produto', 'coluna' => 'nome'),
                        'data' => $produtos,
                        'id' => 'EntradaProdutoId'
                    ),
                    array(
                        'nome' => 'Fornecedor',
                        'dominio' => 'Entrada',
                        'coluna' => 'fornecedor_id',
                        'tipo' => 'hidden',
                        'data' => 0
                    ),
                    array(
                        'nome' => 'Fornecedor',
                        'dominio' => 'Entrada',
                        'coluna' => 'fornecedor',
                        'select' => array('dominio' => 'Fornecedor', 'coluna' => 'nome'),
                        'tipo' => 'autocomplete'
                    ),
                    array(
                        'nome' => 'Quantidade',
                        'dominio' => 'Entrada',
                        'coluna' => 'quantidade',
                        'tipo' => 'text'
                    )
                );
                echo $this->Tabela->imprimir($colunas, $entradas, 'Entrada', array('validationErrors'=>$this->validationErrors, 'habilitarEdicao'=>false, 'habilitarExclusao'=>$habilitarExclusao ,'focusInput'=>'EntradaData') );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>