
<style type="text/css">
    .widget-body {
        height: 300px;
        overflow-y: scroll;
    }
</style>

<div class="row">

    <div class="col-xs-12 col-sm-6 widget-container-span ui-sortable">
        <div class="widget-box">
                <div class="widget-header">
                        <h5 class="bigger lighter">
                                <i class="fa fa-arrow-down"></i>
                                Produtos Abaixo do Estoque Mínimo
                        </h5>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                                <table class="table table-striped table-bordered table-hover">
                                        <thead class="thin-border-bottom">
                                                <tr>
                                                        <th>
                                                            Produto
                                                        </th>

                                                        <th>
                                                            Atual
                                                        </th>
                                                        <th>Mínimo</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            foreach ($produtosAbaixoMinimo as $produto) {
                                            ?>
                                                <tr>
                                                        <td><?php echo $produto['Produto']['nome']; ?></td>

                                                        <td><?php echo $produto['Produto']['estoque']; ?></td>

                                                        <td><?php echo $produto['Produto']['minimo']; ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                </table>
                        </div>
                </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-6 widget-container-span ui-sortable">
        <div class="widget-box">
                <div class="widget-header">
                        <h5 class="bigger lighter">
                                <i class="fa fa-arrow-down"></i>
                                Produtos Abaixo do Estoque de Segurança
                        </h5>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                                <table class="table table-striped table-bordered table-hover">
                                        <thead class="thin-border-bottom">
                                                <tr>
                                                        <th>
                                                            Produto
                                                        </th>

                                                        <th>
                                                            Atual
                                                        </th>
                                                        <th>Segurança</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            foreach ($produtosAbaixoSeguranca as $produto) {
                                            ?>
                                                <tr>
                                                        <td><?php echo $produto['Produto']['nome']; ?></td>

                                                        <td><?php echo $produto['Produto']['estoque']; ?></td>

                                                        <td><?php echo $produto['Produto']['seguranca']; ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                </table>
                        </div>
                </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-6 widget-container-span ui-sortable">
        <div class="widget-box">
                <div class="widget-header">
                        <h5 class="bigger lighter">
                                <i class="fa fa-arrow-up"></i>
                                Produtos Acima do Estoque Máximo
                        </h5>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                                <table class="table table-striped table-bordered table-hover">
                                        <thead class="thin-border-bottom">
                                                <tr>
                                                        <th>
                                                            Produto
                                                        </th>

                                                        <th>
                                                            Atual
                                                        </th>
                                                        <th>Máximo</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                            <?php
                                            foreach ($produtosAcimaMaximo as $produto) {
                                            ?>
                                                <tr>
                                                        <td><?php echo $produto['Produto']['nome']; ?></td>

                                                        <td><?php echo $produto['Produto']['estoque']; ?></td>

                                                        <td><?php echo $produto['Produto']['maximo']; ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                </table>
                        </div>
                </div>
        </div>
    </div>
    
</div>
