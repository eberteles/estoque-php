<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
//                    array(
//                        'nome' => 'Cor',
//                        'dominio' => 'Produto',
//                        'coluna' => 'cor',
//                        'tipo' => 'hidden_cor',
//                        'data' => $cores
//                    ),
                    array(
                        'nome' => 'Nome',
                        'dominio' => 'Produto',
                        'coluna' => 'nome',
                        'tipo' => 'text',
                        'concatenar' => array('dominio'=>'Categoria', 'title'=>'nome','coluna'=>'cor', 'tipo'=>'cor')
                    ),
                    array(
                        'nome' => 'Categoria',
                        'dominio' => 'Produto',
                        'coluna' => 'categoria_id',
                        'tipo' => 'hidden_select',
                        'select' => array('dominio' => 'Categoria', 'coluna' => 'nome'),
                        'data' => $categorias,
                        'id' => 'ProdutoCategoriaId'
                    ),
                    array(
                        'nome' => 'Estoque',
                        'dominio' => 'Produto',
                        'coluna' => 'estoque',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Mínimo',
                        'dominio' => 'Produto',
                        'coluna' => 'minimo',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Segurança',
                        'dominio' => 'Produto',
                        'coluna' => 'seguranca',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Máximo',
                        'dominio' => 'Produto',
                        'coluna' => 'maximo',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Última Alteração',
                        'dominio' => 'Produto',
                        'coluna' => 'usuario_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Usuario', 'coluna' => 'nome'),
                        'input' => false
                    ),
                    array(
                        'nome' => 'Data',
                        'dominio' => 'Produto',
                        'coluna' => 'modified',
                        'tipo' => 'datetime',
                        'input' => false
                    )
                );

                echo $this->Tabela->imprimir($colunas, $produtos, 'Produto', array('validationErrors'=>$this->validationErrors, 'habilitarEdicao'=>$habilitarEdicao, 'habilitarExclusao'=>$habilitarExclusao) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>