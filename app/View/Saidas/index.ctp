<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
                    array(
                        'nome' => 'Saída',
                        'dominio' => 'Saida',
                        'coluna' => 'created',
                        'tipo' => 'text',
                        'input' => false
                    ),
                    array(
                        'nome' => 'Responsável',
                        'dominio' => 'Saida',
                        'coluna' => 'usuario_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Usuario', 'coluna' => 'nome'),
                        'input' => false,
                        'id' => 'SaidaUsuarioId'
                    ),
                    array(
                        'nome' => 'Produto',
                        'dominio' => 'Saida',
                        'coluna' => 'produto_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Produto', 'coluna' => 'nome'),
                        'data' => $produtos,
                        'id' => 'SaidaProdutoId'
                    ),
                    array(
                        'nome' => 'Departamento',
                        'dominio' => 'Saida',
                        'coluna' => 'departamento_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Departamento', 'coluna' => 'nome'),
                        'data' => $departamentos,
                        'id' => 'SaidaDepartamentoId'
                    ),
                    array(
                        'nome' => 'Quantidade',
                        'dominio' => 'Saida',
                        'coluna' => 'quantidade',
                        'tipo' => 'text'
                    )
                );
                echo $this->Tabela->imprimir($colunas, $saidas, 'Saida', array('validationErrors'=>$this->validationErrors, 'habilitarEdicao'=>false, 'habilitarExclusao'=>$habilitarExclusao ) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>