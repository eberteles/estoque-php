<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
                    array(
                        'nome' => 'Departamento',
                        'dominio' => 'Usuario',
                        'coluna' => 'departamento_id',
                        'tipo' => 'select',
                        'select' => array('dominio' => 'Departamento', 'coluna' => 'nome'),
                        'data' => $departamentos,
                        'id' => 'UsuarioDepartamentoId'
                    ),
                    array(
                        'nome' => 'Nome',
                        'dominio' => 'Usuario',
                        'coluna' => 'nome',
                        'tipo' => 'text'
                    ),
                    array(
                        'nome' => 'Usuário',
                        'dominio' => 'Usuario',
                        'coluna' => 'username',
                        'tipo' => 'text',
                        'class' => 'hidden-sm hidden-xs'
                    ),
                    array(
                        'nome' => 'Senha',
                        'dominio' => 'Usuario',
                        'coluna' => 'password',
                        'tipo' => 'password'
                    ),
                    array(
                        'nome' => 'Perfil',
                        'dominio' => 'Usuario',
                        'coluna' => 'role',
                        'tipo' => 'select',
                        'data' => $perfis,
                        'id' => 'UsuarioRole'
                    ),
                    array(
                        'nome' => 'Email',
                        'dominio' => 'Usuario',
                        'coluna' => 'email',
                        'tipo' => 'email',
                        'class' => 'hidden-sm hidden-xs'
                    )
                );
                
                echo $this->Tabela->imprimir($colunas, $usuarios, 'Usuario', array('validationErrors'=>$this->validationErrors) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>false));
?>