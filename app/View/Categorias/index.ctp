<br>
<div class="row">
        <div class="col-xs-12">
            <?php
                $colunas    = array( 
                    array(
                        'nome' => 'Nome',
                        'dominio' => 'Categoria',
                        'coluna' => 'nome',
                        'tipo' => 'text'
                    )
                );
                echo $this->Tabela->imprimir($colunas, $categorias, 'Categoria', array('validationErrors'=>$this->validationErrors, 'threaded'=>true) );
            ?>
        </div>
</div>

<?php
    echo $this->element('rodape_tabela', array('threaded'=>true));
?>