<?php

App::uses('Produto', 'Model');
App::uses('Fornecedor', 'Model');

class Entrada extends AppModel {
    public $name = 'Entrada';
    public $displayField = 'data';
        
    public $validate = array(
        'produto_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Produto.'
            )
        ),
        'fornecedor' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Fornecedor.'
            )
        ),
        'data' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a data da Nota.'
            )
        ),
//        'nota' => array(
//            'required' => array(
//                'rule' => array('notBlank'),
//                'message' => 'Favor informar o Número da Nota.'
//            )
//        ),
        'quantidade' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Quantidade.'
            ),
            'number' => array(
                'rule' => array('range', 0, 50001),
                'message' => 'Favor informar um número entre 1 e 50.000.'
            )
        )
    );
    
    public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id'
        ),
        'Fornecedor' => array(
            'className' => 'Fornecedor',
            'foreignKey' => 'fornecedor_id'
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['usuario_id'] = AuthComponent::user('id');
        if( isset($this->data[$this->name]['fornecedor']) && 
                isset($this->data[$this->name]['fornecedor_id']) && $this->data[$this->name]['fornecedor_id'] == "0" ) {
            
            $this->data[$this->name]['fornecedor']  = mb_strtoupper($this->data[$this->name]['fornecedor'], 'UTF-8');
            $this->data[$this->name]['fornecedor_id'] = $this->Fornecedor->field('id', array('nome'=>$this->data[$this->name]['fornecedor']));
            if($this->data[$this->name]['fornecedor_id'] <= 0) {
                $this->Fornecedor->create();
                $this->Fornecedor->save(array('nome'=>$this->data[$this->name]['fornecedor']));
                $this->data[$this->name]['fornecedor_id'] = $this->Fornecedor->id;
            }
        }
        return parent::beforeSave($options);
    }
    
    public function afterSave($created, $options = array()) {
        if($created) {
            $this->Produto->id = $this->data[$this->name]['produto_id'];
            $this->Produto->saveField('estoque', ($this->Produto->field('estoque') + $this->data[$this->name]['quantidade']) );
            $this->Produto->saveField('usuario_id', AuthComponent::user('id') );
        }
        return parent::afterSave($created, $options = array());
    }
}