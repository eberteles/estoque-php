<?php

class Produto extends AppModel {
    public $name = 'Produto';
    public $displayField = 'combo';
    public $virtualFields = array(
        'combo' => 'CONCAT(Produto.nome, " ( ", Produto.estoque, " )")'
    );
        
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o nome do Produto.'
            )
        ),
        'estoque' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Estoque inicial.'
            ),
            'number' => array(
                'rule' => array('range', -1, 501),
                'message' => 'Favor informar um número entre 0 e 500.'
            )
        ),
        'minimo' => array(
            'number' => array(
                'rule' => array('range', 0, 501),
                'message' => 'Favor informar um número entre 1 e 500.',
                'allowEmpty' => true
            )
        ),
        'seguranca' => array(
            'number' => array(
                'rule' => array('range', 0, 501),
                'message' => 'Favor informar um número entre 1 e 500.',
                'allowEmpty' => true
            )
        ),
        'maximo' => array(
            'number' => array(
                'rule' => array('range', 0, 501),
                'message' => 'Favor informar um número entre 1 e 500.',
                'allowEmpty' => true
            )
        ),
        'categoria_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Categoria.'
            )
        )
    );
    
    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id'
        ),
        'Categoria' => array(
            'className' => 'Categoria',
            'foreignKey' => 'categoria_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['usuario_id'] = AuthComponent::user('id');
        if(isset($this->data['Produto']['nome'])) {
            $this->data['Produto']['nome'] = mb_strtoupper($this->data['Produto']['nome'], 'UTF-8');
        }
        
        return parent::beforeSave($options);
    }
}