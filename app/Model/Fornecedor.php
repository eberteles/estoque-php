<?php

class Fornecedor extends AppModel {
    public $name = 'Fornecedor';
    public $useTable = 'fornecedores';
    public $order = "nome ASC";
    public $displayField = 'nome';
    
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o nome do Fornecedor.'
            )
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['nome']        = mb_strtoupper($this->data[$this->name]['nome'], 'UTF-8');
        
        return parent::beforeSave($options);
    }
    
}