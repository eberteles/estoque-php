<?php

App::uses('Produto', 'Model');

class Saida extends AppModel {
    public $name = 'Saida';
    public $displayField = 'created';
        
    public $validate = array(
        'produto_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Produto.'
            )
        ),
        'departamento_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Departamento.'
            )
        ),
        'quantidade' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Quantidade.'
            ),
            'number' => array(
                'rule' => array('range', 0, 501),
                'message' => 'Favor informar um número entre 1 e 500.'
            )
        )
    );
    
    public $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id'
        ),
        'Departamento' => array(
            'className' => 'Departamento',
            'foreignKey' => 'departamento_id'
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['usuario_id'] = AuthComponent::user('id');
        
        return parent::beforeSave($options);
    }
    
    public function afterSave($created, $options = array()) {
        if($created) {
            $this->Produto->id = $this->data[$this->name]['produto_id'];
            $this->Produto->saveField('estoque', ($this->Produto->field('estoque') - $this->data[$this->name]['quantidade']) );
            $this->Produto->saveField('usuario_id', AuthComponent::user('id') );
        }
        return parent::afterSave($created, $options = array());
    }
}