<?php

class Pedido extends AppModel {
    public $name = 'Pedido';
    public $useTable = 'pedidos';
    public $order = "nome ASC";
    public $displayField = 'nome';
    
    public $validate = array(
        'produto_id' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar o Produto.'
            )
        ),
        'justificativa' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Justificativa.'
            )
        ),
        'solicitado' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'Favor informar a Quantidade.'
            ),
            'number' => array(
                'rule' => array('range', 0, 50001),
                'message' => 'Favor informar um número entre 1 e 50.000.'
            )
        )
    );
    
    public $belongsTo = array(
        'Departamento' => array(
            'className' => 'Departamento',
            'foreignKey' => 'departamento_id'
        ),
        'Solicitante' => array(
            'className' => 'Usuario',
            'foreignKey' => 'solicitante_id'
        ),
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id'
        ),
        'Aprovador' => array(
            'className' => 'Usuario',
            'foreignKey' => 'aprovador_id'
        )
    );
    
    public function beforeSave($options = array())
    {
        $this->data[$this->name]['justificativa']        = mb_strtoupper($this->data[$this->name]['justificativa'], 'UTF-8');
        
        return parent::beforeSave($options);
    }
    
}