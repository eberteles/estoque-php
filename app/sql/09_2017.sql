--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(10) NOT NULL,
  `departamento_id` int(10) NOT NULL,
  `solicitante_id` int(10) NOT NULL,
  `produto_id` int(10) NOT NULL,
  `aprovador_id` int(10) DEFAULT NULL,
  `justificativa` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `solicitado` int(10) NOT NULL,
  `aprovado` int(10) DEFAULT NULL,
  `created` datetime NOT NULL,
  `saida` datetime DEFAULT NULL
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Tabela de Pedidos';

ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `pedidos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;


ALTER TABLE `usuarios` ADD `email` VARCHAR(255) NULL AFTER `role`;