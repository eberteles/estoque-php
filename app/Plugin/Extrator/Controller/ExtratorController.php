<?php

App::uses('File', 'Utility');
App::uses('Folder', 'Utility');

define('DADOS', ROOT . DS . 'dados' . DS);

class ExtratorController extends ExtratorAppController {
    
    public function index() {
        $this->condiminios();
    }
    
    private function condiminios() {
        debug('iniciar');
    
        $file = new File(DADOS . 'DadosDadGera.txt');

        $contents = $file->read();
        //$convert = explode("\n", utf8_encode($contents));
        $convert = explode("\n", $contents);
      
        $configuracao   = array(
            'codigo'    => array(0, 3),
            'nome'      => array(3, 35),
            'endereco'  => array(38, 35),
            'bairro'    => array(73, 15),
            'cidade'    => array(88, 15),
            'uf'        => array(103, 2),
            'cep'       => array(105, 8),
            'telefone'  => array(113, 9),
            'cnpj'      => array(122, 14),
            'sindico'   => array(136, 31),
            'unidade'   => array(167, 5),
            'data_ver'  => array(172, 10),
            'tp_doc'    => array(182, 4),
            'documento' => array(186, 14),
            'sexo'      => array(200, 1),
            'sim_nao'   => array(201, 1),
            'email'   => array(202, 50)
        );
        $condominios = array();
        foreach ($convert as $condominio) {
            if(strlen($condominio) > 100) {
                $condominios[] = $this->lineToObject($condominio, $configuracao, 'Condominio');
            }
        }
        //debug($condominios);

        $file->close();
        
    }
  
    /**
    * @param string $line Linha com os dados
    * @param Array $configuracao Formato dos dados da linha array( 'condominio' => array(0, 10)  )
    */
    private function lineToObject($line, $configuracao, $objeto) {
        $retorno = array();
        foreach( $configuracao as $chave => $valor ) {
            $retorno[$objeto][$chave] = trim(mb_substr($line, $valor[0], $valor[1]));
        }
        debug(strlen($line));
        debug($retorno);
        return $retorno;
    }

}