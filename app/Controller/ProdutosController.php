<?php

class ProdutosController extends AppController {
    
    public $cores = array(
        '#ac725e' => '#ac725e',
        '#d06b64' => '#d06b64',
        '#f83a22' => '#f83a22',
        '#fa573c' => '#fa573c',
        '#ff7537' => '#ff7537',
        '#ffad46' => '#ffad46',
        '#42d692' => '#42d692',
        '#16a765' => '#16a765',
        '#7bd148' => '#7bd148',
        '#b3dc6c' => '#b3dc6c',
        '#fbe983' => '#fbe983',
        '#fad165' => '#fad165',
        '#92e1c0' => '#92e1c0',
        '#9fe1e7' => '#9fe1e7',
        '#9fc6e7' => '#9fc6e7',
        '#4986e7' => '#4986e7',
        '#9a9cff' => '#9a9cff',
        '#b99aff' => '#b99aff',
        '#c2c2c2' => '#c2c2c2',
        '#cabdbf' => '#cabdbf',
        '#cca6ac' => '#cca6ac',
        '#f691b2' => '#f691b2',
        '#cd74e6' => '#cd74e6',
        '#a47ae2' => '#a47ae2',
        '#555' => '#555'
    );
    
    public function index() {
        
        if (isset($this->data['Produto']['id'])) {
            $this->Produto->create();
            if ($this->Produto->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $conditions = array();
        if (isset($this->data['pesquisar'])) {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');            
            $this->Session->write(array('pesquisar-produtos'=>$pesquisa));
        }
        if( $this->Session->read('pesquisar-' . $this->params['controller']) != null ) {
            $pesquisa = $this->Session->read('pesquisar-' . $this->params['controller']);
            $conditions['or'] = array(
                'Produto.nome like' => '%' . $pesquisa . '%',
                'Usuario.nome like' => '%' . $pesquisa . '%',
                'Categoria.nome like' => '%' . $pesquisa . '%'
            );
            $this->set('pesquisar', $pesquisa);
        }
        
        $this->set('produtos', $this->Produto->find('all', array(
            'conditions' => $conditions,
            'order' => 'Produto.nome ASC'
        )));
        
        $this->set('categorias', $this->Produto->Categoria->find('list', array(
            'order' => 'Categoria.nome ASC'
        )));
        
        $this->set('cores', $this->cores);
        
        $habilitarEdicao    = true;
        $habilitarExclusao  = true;
        if($this->Auth->user('role') == 'funcionario') {
            $habilitarEdicao    = false;
            $habilitarExclusao  = false;
        }
        $this->set('habilitarEdicao', $habilitarEdicao);
        $this->set('habilitarExclusao', $habilitarExclusao);
    }
    
    public function delete()
    {
        if (isset($this->data['Produto']['id'])) {
            if ($this->Produto->delete($this->data['Produto']['id'])) {
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'gerente' || $user['role'] === 'funcionario') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>