<?php

class EntradasController extends AppController {
       
    public function index() {
        
        if (isset($this->data['Entrada']['id'])) {
            $this->Entrada->create();
            if ($this->Entrada->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $conditions = array();
        if (isset($this->data['pesquisar'])) {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');
            $this->Session->write(array('pesquisar-' . $this->params['controller']=>$pesquisa));
        }
        if( $this->Session->read('pesquisar-' . $this->params['controller']) != null ) {
            $pesquisa = $this->Session->read('pesquisar-' . $this->params['controller']);            
            $conditions['or'] = array(
                'Entrada.nota like' => '%' . $pesquisa . '%',
                'Produto.nome like' => '%' . $pesquisa . '%',
                'Usuario.nome like' => '%' . $pesquisa . '%',
                'Fornecedor.nome like' => '%' . $pesquisa . '%'
            );
            $this->set('pesquisar', $pesquisa);
        }
        
        $this->set('produtos', $this->Entrada->Produto->find('list', array(
            'order' => 'nome ASC'
        )));
        
        $this->set('entradas', $this->Entrada->find('all', array(
            'conditions' => $conditions,
            'order' => 'data DESC',
        )));
        
        $habilitarExclusao  = true;
        if($this->Auth->user('role') == 'funcionario') {
            $habilitarExclusao  = false;
        }
        $this->set('habilitarExclusao', $habilitarExclusao);
        
    }
    
    public function delete()
    {
        if (isset($this->data['Entrada']['id'])) {
            $this->Entrada->id  = $this->data['Entrada']['id'];
            $entrada    = $this->Entrada->read();
            if ($this->Entrada->delete($this->data['Entrada']['id'])) {
                $this->afterDelete($entrada);
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    private function afterDelete($entrada) {
        $this->loadModel('Produto');
        $this->Produto->id = $entrada['Entrada']['produto_id'];
        $this->Produto->saveField('estoque', ($this->Produto->field('estoque') - $entrada['Entrada']['quantidade']) );
        $this->Produto->saveField('usuario_id', AuthComponent::user('id') );
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'gerente' || $user['role'] === 'funcionario') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>