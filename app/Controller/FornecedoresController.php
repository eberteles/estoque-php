<?php

class FornecedoresController extends AppController {
    
    var $uses = array('Fornecedor');
    
    public function pesquisar() {
        $this->autoRender = false;
        
        if( isset( $this->data['nome'] ) ) {
            echo json_encode( $this->Fornecedor->find('list', array( 
                'conditions' => array( 'nome like ' => '%' . $this->data['nome'] . '%' ),
                'order' => 'nome ASC' ) ) 
                );
        }
        exit();
    }
    
}

?>