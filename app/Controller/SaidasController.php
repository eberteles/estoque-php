<?php

class SaidasController extends AppController {
       
    public function index() {
        
        if (isset($this->data['Saida']['id'])) {
            $this->Saida->create();
            if ($this->Saida->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $this->set('produtos', $this->Saida->Produto->find('list', array(
            'order' => 'nome ASC'
        )));
        
        $this->set('departamentos', $this->Saida->Departamento->find('list', array(
            'order' => 'nome ASC'
        )));
        
        $conditions = array();
        if (isset($this->data['pesquisar'])) {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');
            $this->Session->write(array('pesquisar-' . $this->params['controller']=>$pesquisa));
        }
        if( $this->Session->read('pesquisar-' . $this->params['controller']) != null ) {
            $pesquisa = $this->Session->read('pesquisar-' . $this->params['controller']);
            $conditions['or'] = array(
                'Produto.nome like' => '%' . $pesquisa . '%',
                'Usuario.nome like' => '%' . $pesquisa . '%',
                'Departamento.nome like' => '%' . $pesquisa . '%'
            );
            $this->set('pesquisar', $pesquisa);
        }
        
        $this->set('saidas', $this->Saida->find('all', array(
            'conditions' => $conditions,
            'order' => 'Saida.created DESC',
        )));
        
        $habilitarExclusao  = true;
        if($this->Auth->user('role') == 'funcionario') {
            $habilitarExclusao  = false;
        }
        $this->set('habilitarExclusao', $habilitarExclusao);
        
    }
    
    public function delete()
    {
        if (isset($this->data['Saida']['id'])) {
            $this->Saida->id    = $this->data['Saida']['id'];
            $saida  = $this->Saida->read();
            if ($this->Saida->delete($this->data['Saida']['id'])) {
                $this->afterDelete($saida);
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    private function afterDelete($saida) {
        $this->loadModel('Produto');
        $this->Produto->id = $saida['Saida']['produto_id'];
        $this->Produto->saveField('estoque', ($this->Produto->field('estoque') + $saida['Saida']['quantidade']) );
        $this->Produto->saveField('usuario_id', AuthComponent::user('id') );
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'gerente' || $user['role'] === 'funcionario') {
                return true;
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>