<?php

App::uses('CakeEmail', 'Network/Email');

class UsuariosController extends AppController {

//    public function beforeFilter() {
//        parent::beforeFilter();
//        $this->Auth->allow('index');
//    }
    
    public $perfis = array(
            'admin'  => 'Administrador',
            'gerente' => 'Gerente Operacional',
            'funcionario' => 'Controle Estoque',
            'solicitante' => 'Solicitante'
        );
    
    public function index() {
        
        $this->Usuario->recursive = 2;
        $this->paginate = array(
            'limit' => 20
        );
        $conditions = array();
        if ( $this->Auth->user('role') != 'admin' ) {
            $conditions = array( 'cliente_id' => $this->Auth->user('cliente_id'), 'role !='=>'admin' );
        }
        
        if (isset($this->data['Usuario']['id'])) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
                
        if (isset($this->data['pesquisar'])) {
            $pesquisa   = mb_strtoupper($this->data['pesquisar'], 'UTF-8');
            $this->Session->write(array('pesquisar-' . $this->params['controller']=>$pesquisa));
        }
        if( $this->Session->read('pesquisar-' . $this->params['controller']) != null ) {
            $pesquisa = $this->Session->read('pesquisar-' . $this->params['controller']);
            $conditions['or'] = array(
                'Usuario.nome like' => '%' . $pesquisa . '%',
                'Usuario.username like' => '%' . $pesquisa . '%',
                'Departamento.nome like' => '%' . $pesquisa . '%'
            );
            $this->set('pesquisar', $pesquisa);
        }
        $this->set('usuarios', $this->paginate($conditions));
        
        $this->set('departamentos', $this->Usuario->Departamento->find('list', array(
            'order' => 'nome ASC'
        )));
        
        $this->set('perfis', $this->perfis);        
    }
    
    public function delete()
    {
        if (isset($this->data['Usuario']['id'])) {
            if ($this->Usuario->delete($this->data['Usuario']['id'])) {
                $this->Session->setFlash('Registro excluído com sucesso', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Erro ao excluir o registro');
            }
        } else {
            $this->Session->setFlash('Não foi possível excluir o registro');
        }
        $this->redirect(array(
            'action' => 'index'
        ));
    }
    
    public function login() {
        $this->layout   = 'login';
        if ($this->Auth->login()) {
            $agora  = new DateTime('now');
            $this->Usuario->id = $this->Auth->user('id');
            $this->Usuario->saveField("last_login", $agora->format('Y-m-d H:i:s'), array('callbacks'=>false));
            $this->redirect($this->Auth->redirect());
        } else {
            if($this->request->is('post')) {
                if (isset($this->data['Usuario']['email'])) {
                    $usuario    = $this->Usuario->find('first', array(
                        'conditions' => array('Usuario.email' => $this->data['Usuario']['email'])
                    ));
                    if (isset($usuario['Usuario']['id'])) {
                        $novaSenha  = $this->geraSenha();
                        $this->Usuario->id = $usuario['Usuario']['id'];
                        $this->Usuario->saveField("password", $novaSenha);
                        $this->enviarSenha($usuario, $novaSenha);
                        $this->Flash->error('Nova senha encaminhada para o e-mail cadastrado.');
                    }
                    else {
                        $this->Flash->error('E-mail não cadastrado, tente novamente.');
                    }
                }
                else {
                    $this->Flash->error('Usuário ou senha inválido, tente novamente.');
                }
            }
        }
    }
    
    public function sair() {
        $this->redirect($this->Auth->logout());
    }
    
    public function perfil() {
        if (isset($this->data['Usuario']['id'])) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->request->data)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class'=>'message success'));
            } else {
                $this->Session->setFlash('Não foi possível salvar. Tente novamente.');
            }
        }
        
        $this->data = $this->Usuario->read(null, $this->Auth->user('id'));
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'gerente') {
                return true;
            } else {
                if ($this->action === 'perfil' || $this->action === 'sair') {
                    return true;
                }
            }
        }
        if ($this->action === 'login') {
            return true;
        }
        $this->redirect($this->Auth->redirect());
    }
    
    public function getLogo() {
        echo $this->Auth->user('Cliente.logo');
        exit();
    }
    
    private function enviarSenha($usuario, $novaSenha) {
        $assunto    = "[EBAC Estoque] Alteração de senha do usuário - " . $usuario['Usuario']['username'];
        $mensagem   = $usuario['Usuario']['nome'] . ",<br>" . 
            "==================================================" . "<br>" . 
            "Foi solicitado a alteração dos dados de acesso do seu usuário ao Sistema de Estoque.<br>" .
            "Segue abaixo novos dados:<br>" .
            "Usuário: " . $usuario['Usuario']['username']  . "<br>" . 
            "Senha: " . $novaSenha  . "<br>" . 
            "==================================================";
        
        $email = new CakeEmail();
        $email->to(array($usuario['Usuario']['email'] => $usuario['Usuario']['nome']));
        $email->subject($assunto);
        $email->send($mensagem);
    }
    
    private function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        // Caracteres de cada tipo
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        // Variáveis internas
        $retorno = '';
        $caracteres = '';
        // Agrupamos todos os caracteres que poderão ser utilizados
        $caracteres .= $lmin;
        if ($maiusculas) $caracteres .= $lmai;
        if ($numeros) $caracteres .= $num;
        if ($simbolos) $caracteres .= $simb;
        // Calculamos o total de caracteres possíveis
        $len = strlen($caracteres);
        for ($n = 1; $n <= $tamanho; $n++) {
            // Criamos um número aleatório de 1 até $len para pegar um dos caracteres
            $rand = mt_rand(1, $len);
            // Concatenamos um dos caracteres na variável $retorno
            $retorno .= $caracteres[$rand-1];
        }
        return $retorno;
    }
}

?>