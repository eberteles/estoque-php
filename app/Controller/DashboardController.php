<?php

class DashboardController extends AppController {
    
    
    public function index() {
        
        $this->loadModel('Produto');
        
        $this->set('produtosAbaixoMinimo', 
                $this->Produto->find('all', array(
                    'conditions' => array( 'Produto.estoque < Produto.minimo' ),
                    'order' => 'Produto.nome ASC' ) )
                );
        
        $this->set('produtosAbaixoSeguranca', 
                $this->Produto->find('all', array(
                    'conditions' => array( 'Produto.estoque < Produto.seguranca' ),
                    'order' => 'Produto.nome ASC' ) )
                );
        
        $this->set('produtosAcimaMaximo', 
                $this->Produto->find('all', array(
                    'conditions' => array( 'Produto.estoque > Produto.maximo' ),
                    'order' => 'Produto.nome ASC' ) )
                );

    }
    
    private function getUltimos6Meses() {
        $datas  = array();
        $agora  = new DateTime('now');
        $agora->sub(new DateInterval("P5M"));
        
        $datas['meses'][] = $agora->format('m');
        $datas['anos'][] = $agora->format('Y');
        $datas['siglas'][] = $this->getSiglaMes($agora->format('m'));
        
        for($i=0; $i<5; $i++) {
            $agora->add(new DateInterval("P1M"));
            $datas['meses'][] = $agora->format('m');
            $datas['anos'][] = $agora->format('Y');
            $datas['siglas'][] = $this->getSiglaMes($agora->format('m'));
        }                
        return $datas;
    }
    
    private function getSiglaMes($mes) {
        switch ($mes) {
                case "01": return '"Jan"';
                case "02": return '"Fev"';
                case "03": return '"Mar"';
                case "04": return '"Abr"';
                case "05": return '"Mai"';
                case "06": return '"Jun"';
                case "07": return '"Jul"';
                case "08": return '"Ago"';
                case "09": return '"Set"';
                case "10": return '"Out"';
                case "11": return '"Nov"';
                case "12": return '"Dez"';
         }
    }
    
    public function pedido() {
        
    }
    
    public function isAuthorized($user) {
        if (parent::isAuthorized($user)) {
            if ($user['role'] === 'admin' || $user['role'] === 'gerente' || $user['role'] === 'funcionario') {
                return true;
            }
            if ($user['role'] === 'solicitante') {
                if ($this->action === 'pedido') {
                    return true;
                } else {
                    $this->redirect(array(
                        'action' => 'pedido'
                    ));
                }
            }
        }
        $this->redirect($this->Auth->redirect());
    }
    
}

?>